﻿namespace Hte.Models
{
    public class SendSmsResult
    {
        public string Result;
        public int Status;
        public bool Ok;
    }
}