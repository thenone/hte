﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class Gift
    {
        public Gift()
        {
            DrawChanceIds = new List<string>();
        }
        
        [BindNever]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BindNever]
        public string Code { get; set; } = Guid.NewGuid().ToString();
        
        [Required]
        [BsonRequired]
        [BsonElement]
        public string Title { get; set; }
        
        [Required]
        [BsonRequired]
        [BsonElement]
        public string Price { get; set; }
        
        [FromForm(Name = "date_bought")]
        [BsonRequired]
        [BsonElement]
        public DateTime DateBought { get; set; }
        
        [BindNever]
        [BsonRequired]
        [BsonElement]
        public DateTime DateDrawed { get; set; }
        
        [BindNever]
        [BsonRequired]
        [BsonElement]
        public DateTime DateRegistered { get; set; } = DateTime.Now;
        
        [BindNever]
        [BsonRequired]
        [BsonElement]
        public bool Drawed { get; set; } = false;
        
        [Required]
        [FromForm(Name = "required_points")]
        [BsonRequired]
        [BsonElement]
        public int RequiredPoints { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRepresentation(BsonType.ObjectId)]
        public string WinnerId { get; set; }
        
        [BindNever]
        [BsonIgnore]
        public User Winner { get; set; }
        
        [BindNever]
        [BsonElement]
        public IEnumerable<string> DrawChanceIds { get; set; }
        
        [BindNever]
        [BsonIgnore]
        public IEnumerable<DrawChance> DrawChances { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonIgnoreIfDefault]
        public IList<Photo> Photos { get; set; }
        
        [BindRequired]
        [BsonIgnore]
        public IEnumerable<IFormFile> Pics { get; set; }
    }

    public enum GiftDrawingStatus
    {
        Undrawn,
        All,
        Drawn
    }
}