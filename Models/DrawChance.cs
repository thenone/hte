﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class DrawChance
    {
        [BindNever]
        [BsonElement]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Gift { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        [BsonRepresentation(BsonType.ObjectId)]
        public string User { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public int GiftUnitPoint { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public int TotalPoints { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public int Quantity { get; set; }
        
        [BindNever]
        [BsonElement]
        [BsonRequired]
        public DateTime DateBought { get; set; }
    }
}