﻿namespace Hte.Models.Sms
{
    public class PtpSend
    {
        public PtpSend(string username, string password)
        {
            Username = username;
            Password = password;
        }
        
        public string Username { get; set; }
        public string Password { get; set; }
        public SendType SendType { get; set; }
        public string YourMessageIds { get; set; }
        public string Message { get; set; }
        public string Numbers { get; set; }
        public string SenderNumber { get; set; }
    }

    public enum SendType
    {
        Normal = 1,
        Flash = 2
    }
}