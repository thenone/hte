﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class Notification
    {
        [BindNever]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [Required]
        [BsonRequired]
        [BsonElement]
        public string Title { get; set; }
        
        [Required]
        [BsonRequired]
        [BsonElement]
        public string Description { get; set; }
        
        [FromQuery]
        [BsonRequired]
        [BsonElement]
        public NotificationType Type { get; set; }
        
        [BindNever]
        [BsonRequired]
        [BsonElement]
        public IEnumerable<string> UsersId { get; set; }

        [BindNever]
        [BsonRequired]
        [BsonElement]
        public IEnumerable<string> SeenUserIds { get; set; } = new List<string>(); 

        [BindNever]
        [BsonIgnore]
        public IEnumerable<User> Users { get; set; }
    }

    public enum NotificationType
    {
        User = 1,
        Group = 2
    }

    public enum NotificationSeenStatus
    {
        Unseen,
        All,
        Seen
    }
}