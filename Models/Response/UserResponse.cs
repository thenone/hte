﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models.Response
{
    public class UserResponse
    {
        [BindNever]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement]
        [BsonIgnoreIfNull]
        public string FirstName { get; set; }
        
        [BsonElement]
        [BsonIgnoreIfNull]
        public string LastName { get; set; }

        [BsonElement]
        public int Points { get; set; } = 0;

        [BindNever]
        [BsonElement]
        [BsonRequired]
        public UserStatus Status { get; set; } = UserStatus.New;
        
        [BindNever]
        [BsonElement]
        public Photo Avatar { get; set; }
    }
}