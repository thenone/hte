﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hte.Models
{
    public class Post
    {
        [BindNever]
        [BsonId]
        [BsonElement]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BindNever]
        [BsonElement]
        public string Code { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [BsonElement]
        [BsonRequired]
        public string Title { get; set; }
        
        [Required]
        [BsonRequired]
        [BsonElement]
        public string Description { get; set; }
        
        [BindNever]
        [BsonElement]
        public DateTime DateCreated { get; set; } = DateTime.Now;
        
        [BindNever]
        [BsonElement]
        [BsonIgnoreIfDefault]
        public IList<Photo> Media { get; set; }
        
        [BsonIgnore]
        public IEnumerable<IFormFile> Files { get; set; } 
    }
}