﻿namespace Hte.Models
{
    public class TokenOptions
    {
        public string Issuer;
        public string Audience;
        public string Key;
        public string ExpiresTimeInMinutes;
    }
}