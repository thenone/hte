﻿using MongoDB.Driver;

namespace Hte.Models.Context
{
    public interface IAppContext
    {
        IMongoCollection<User> Users { get; }
        IMongoCollection<Gift> Gifts { get; }
        IMongoCollection<DrawChance> DrawChances { get; }
        IMongoCollection<UserGroup> UserGroups { get; }
        IMongoCollection<Notification> Notifications { get; }
        IMongoCollection<UserReport> UserReportComplaints { get; }
        IMongoCollection<Post> Posts { get; }
        IMongoCollection<TrashCollecting> Collectings { get; }
    }
}