﻿using System.Collections.Generic;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Hte.Models.Context
{
    public class AppContext : IAppContext
    {
        private bool _isIndexingNeeded = true;
        private readonly IMongoDatabase _db;
        
        public AppContext(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            _db = client.GetDatabase(options.Value.Database);

            CreateIndexes();
        }

        private async void CreateIndexes()
        {
            if (!_isIndexingNeeded)
            {
                return;
            }
            _isIndexingNeeded = false;
            
            var usersIndexes = new Dictionary<IndexKeysDefinition<User>, bool>
            {
                {Builders<User>.IndexKeys.Ascending(u => u.Mobile), true},
                {Builders<User>.IndexKeys.Ascending(u => u.Username), true},
                {Builders<User>.IndexKeys.Ascending(u => u.Email), true}
            };

            var giftsIndexes = new Dictionary<IndexKeysDefinition<Gift>, bool>
            {
                {Builders<Gift>.IndexKeys.Ascending(g => g.Code), true},
                {Builders<Gift>.IndexKeys.Ascending(g => g.DateRegistered), true},
                {Builders<Gift>.IndexKeys.Ascending(g => g.Title), false}
            };
            
            var drawChancesIndexes = new Dictionary<IndexKeysDefinition<DrawChance>, bool>
            {
                {Builders<DrawChance>.IndexKeys.Ascending(dc => dc.Gift), false},
                {Builders<DrawChance>.IndexKeys.Ascending(dc => dc.Quantity), false}
            };
            
            var userGroupIndexes = new Dictionary<IndexKeysDefinition<UserGroup>, bool>
            {
                {Builders<UserGroup>.IndexKeys.Ascending(ug => ug.Name), false}
            };
            
            var notificationsIndexes = new Dictionary<IndexKeysDefinition<Notification>, bool>
            {
                {Builders<Notification>.IndexKeys.Ascending(n => n.Type), false},
                {Builders<Notification>.IndexKeys.Ascending(n => n.Title), false}
            };
            
            var userReportComplaintsIndexes = new Dictionary<IndexKeysDefinition<UserReport>, bool>
            {
                {Builders<UserReport>.IndexKeys.Ascending(ur => ur.Title), false}
            };
            
            foreach (var index in usersIndexes)
            {
                try
                {
                    await Users.Indexes.CreateOneAsync(
                        new CreateIndexModel<User>(
                            index.Key,
                            new CreateIndexOptions {Unique = index.Value}
                        )
                    );
                }
                catch (MongoCommandException)
                {
                }
            }
            
            foreach (var index in giftsIndexes)
            {
                try
                {
                    await Gifts.Indexes.CreateOneAsync(
                        new CreateIndexModel<Gift>(
                            index.Key,
                            new CreateIndexOptions {Unique = index.Value}
                        )
                    );
                }
                catch (MongoCommandException)
                {
                }
            }
            
            foreach (var index in drawChancesIndexes)
            {
                try
                {
                    await DrawChances.Indexes.CreateOneAsync(
                        new CreateIndexModel<DrawChance>(
                            index.Key,
                            new CreateIndexOptions {Unique = index.Value}
                        )
                    );
                }
                catch (MongoCommandException)
                {
                }
            }
            
            foreach (var index in userGroupIndexes)
            {
                try
                {
                    await UserGroups.Indexes.CreateOneAsync(new CreateIndexModel<UserGroup>(index.Key, new CreateIndexOptions {Unique = index.Value}));                    
                }
                catch (MongoCommandException)
                {
                }
            }
            
            foreach (var index in notificationsIndexes)
            {
                try
                {
                    await Notifications.Indexes.CreateOneAsync(
                        new CreateIndexModel<Notification>(index.Key, new CreateIndexOptions {Unique = index.Value}));
                }
                catch (MongoCommandException)
                {
                }
            }
            
            foreach (var index in userReportComplaintsIndexes)
            {
                try
                {
                    await UserReportComplaints.Indexes.CreateOneAsync(new CreateIndexModel<UserReport>(index.Key, new CreateIndexOptions {Unique = index.Value}));
                }
                catch (MongoCommandException)
                {
                }
            }
        }

        public IMongoCollection<User> Users => _db.GetCollection<User>("Users");
        public IMongoCollection<DrawChance> DrawChances => _db.GetCollection<DrawChance>("DrawChances");
        public IMongoCollection<Gift> Gifts => _db.GetCollection<Gift>("Gifts");
        public IMongoCollection<UserGroup> UserGroups => _db.GetCollection<UserGroup>("UserGroups");
        public IMongoCollection<Notification> Notifications => _db.GetCollection<Notification>("Notifications");
        public IMongoCollection<UserReport> UserReportComplaints => _db.GetCollection<UserReport>("Reports");
        public IMongoCollection<Post> Posts => _db.GetCollection<Post>("Posts");
        public IMongoCollection<TrashCollecting> Collectings => _db.GetCollection<TrashCollecting>("TrashCollectings");
    }
}