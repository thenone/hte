﻿using System;
using System.Security.Cryptography;

namespace Hte.Helpers.RandomHelper
{
    public class RandomGenerator : IRandomGenerator
    {
        private readonly Random _random;
        private readonly RNGCryptoServiceProvider _rngCryptoServiceProvider;

        public RandomGenerator(RNGCryptoServiceProvider rngCryptoServiceProvider)
        {
            _random = new Random();
            _rngCryptoServiceProvider = rngCryptoServiceProvider;
        }
        
        public int GetRandomInt(int min, int max)
        {
            return _random.Next(min, max);
        }
        
        public int GetRandomInt(int max)
        {
            return _random.Next(max);
        }

        public string GetRandomNumericalString(int length)
        {
            var res = new string[length];
            res[0] = ((char) _random.Next(49, 58)).ToString();
            for (var i = 1; i < length; i ++)
            {
                res[i] = ((char) _random.Next(48, 58)).ToString();
            }

            return string.Join("", res);
        }

        public string GetRandomString(int length)
        {
            var res = new string[length];
            for (var i = 0; i < length; i ++)
            {
                res[i] = GetRandomCapsChar().ToString();
            }

            return string.Join("", res);
        }

        public string GetRandomString(int length, RandomStringCaps caps)
        {
            var res = new string[length];
            if (caps == RandomStringCaps.LowerCase)
            {
                for (var i = 0; i < length; i++)
                {
                    res[i] = GetRandomChar(RandomStringCaps.LowerCase).ToString();
                }
            }
            else
            {
                for (var i = 0; i < length; i++)
                {
                    res[i] = GetRandomChar(RandomStringCaps.UpperCase).ToString();
                }
            }

            return string.Join("", res);
        }

        public int GetCryptoRandomInt()
        {
            var randomNumber = new byte[1];
            _rngCryptoServiceProvider.GetBytes(randomNumber);
            return randomNumber[0];
        }

        /* Helpers */

        private char GetRandomCapsChar()
        {
            return _random.Next(2) == 1
                ? GetRandomChar(RandomStringCaps.UpperCase)
                : GetRandomChar(RandomStringCaps.LowerCase);
        }

        private char GetRandomChar(RandomStringCaps caps)
        {
            return caps == RandomStringCaps.UpperCase ? (char) _random.Next(65, 91) : (char) _random.Next(97, 123);
        }
    }
}