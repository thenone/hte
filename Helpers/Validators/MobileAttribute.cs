﻿using System.ComponentModel.DataAnnotations;

namespace Hte.Helpers.Validators
{
    public class MobileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            // Regex-based validation:
            // var regex = new Regex(@"^0??9\d{9}$");
            // var match = regex.Match(input);
            // return match.Success;
            
            if (string.IsNullOrEmpty((string) value))
            {
                return false;
            }
            var input = value.ToString();

            switch (input.Length)
            {
                case 11:
                {
                    if (input.Substring(0, 2) != "09")
                    {
                        return false;
                    }

                    input = input.Substring(1);

                    break;
                }
                case 10:
                {
                    if (input.Substring(0, 1) != "9")
                    {
                        return false;
                    }

                    break;
                }
                default:
                {
                    return false;
                }
            }

            foreach (var ch in input)
            {
                if (!int.TryParse(ch.ToString(), out _))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
