﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace Hte.Helpers.Validators
{
    public class MongoObjectIdAttribute : ValidationAttribute
    {
        public MongoObjectIdAttribute()
        {
            ErrorMessage = "Invalid id";
        }
        
        public override bool IsValid(object value)
        {
            return ObjectId.TryParse(value.ToString(), out _);
        }
    }
}