﻿using Hte.Models;

namespace Hte.Helpers.Factories
{
    public interface IUserFactory
    {
        User CreateUser();
    }
}