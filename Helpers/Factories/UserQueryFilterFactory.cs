﻿using System.Collections.Generic;
using MongoDB.Driver;
using Hte.Models;

namespace Hte.Helpers.Factories
{
    public class UserQueryFilterFactory : IUserQueryFilterFactory
    {
        public IList<FilterDefinition<User>> CreateDefaultFilters()
        {
            var filters = new List<FilterDefinition<User>>();
            var filterBuilder = new FilterDefinitionBuilder<User>();
            filters.Add(filterBuilder.Gt(u => u.Status, UserStatus.Deactive));
            return filters;
        }
    }
}