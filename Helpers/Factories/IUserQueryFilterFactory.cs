﻿using System.Collections.Generic;
using MongoDB.Driver;
using Hte.Models;

namespace Hte.Helpers.Factories
{
    public interface IUserQueryFilterFactory
    {
        IList<FilterDefinition<User>> CreateDefaultFilters();
    }
}