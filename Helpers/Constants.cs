﻿namespace Hte.Helpers
{
    public struct Constants
    {
        public struct Policies
        {
            public const string MustBeAdmin = "MustBeAdmin";
            public const string MustBeUser = "MustBeUser";
        }

        public struct Roles
        {
            public const string Admin = "Admin";
            public const string User = "User";
        }
    }
}