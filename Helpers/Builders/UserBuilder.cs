﻿using Hte.Helpers.Factories;
using Hte.Models;

namespace Hte.Helpers.Builders
{
    public class UserBuilder : IUserBuilder
    {
        private readonly IUserFactory _userFactory;
        private User _user; 
        

        public UserBuilder(IUserFactory userFactory)
        {
            _userFactory = userFactory;
        }

        public IUserBuilder CreateUser()
        {
            _user = _userFactory.CreateUser();
            return this;
        }

        public IUserBuilder SetUserName(string username)
        {
            _user.Username = username;
            return this;
        }

        public IUserBuilder SetPassword(string password)
        {
            _user.PasswordHash = password;
            return this;
        }

        public IUserBuilder SetMobile(string mobile)
        {
            _user.Mobile = mobile;
            return this;
        }

        public IUserBuilder SetFirstName(string firstName)
        {
            _user.FirstName = firstName;
            return this;
        }

        public IUserBuilder SetLastName(string lastname)
        {
            _user.LastName = lastname;
            return this;
        }

        public IUserBuilder SetEmail(string email)
        {
            _user.Email = email;

            return this;
        }

        public IUserBuilder SetRole(string role)
        {
            _user.Role = role;
            
            return this;
        }

        public User Build()
        {
            return _user;
        }
    }
}