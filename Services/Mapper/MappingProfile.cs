﻿using AutoMapper;
using Hte.Models;
using Hte.Models.Form;
using Hte.Models.Response;

namespace Hte.Services.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserResponse>();
            CreateMap<UserEditForm, User>();
        }
    }
}