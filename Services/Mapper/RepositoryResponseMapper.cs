﻿using System.Collections.Generic;
using AutoMapper;
using Hte.Models;
using System.Linq;
using Hte.Models.Form;
using Hte.Models.Response;

namespace Hte.Services.Mapper
{
    public class RepositoryResponseMapper : IRepositoryResponseMapper
    {
        private readonly IMapper _mapper;

        public RepositoryResponseMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public UserResponse MapUserResponse(User user)
        {
            return _mapper.Map<User, UserResponse>(user);
        }

        public User MapEditForm(UserEditForm editForm)
        {
            return _mapper.Map<UserEditForm, User>(editForm);
        }

        public IEnumerable<UserResponse> MapUserResponses(IEnumerable<User> users)
        {
            return from user in users select MapUserResponse(user);
        }
    }
}