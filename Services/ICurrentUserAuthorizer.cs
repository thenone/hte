﻿using System.Security.Claims;

namespace Hte.Services
{
    public interface ICurrentUserAuthorizer
    {
        bool IsSelfUserAuthorized(ClaimsPrincipal user, string requestedUserId);
        string GetUserId(ClaimsPrincipal user);
    }
}