﻿using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Services.Messaging
{
    public interface ISmsSender
    {
        Task<SendSmsResult> SendLoginCode(string code, string to);
        Task<SendSmsResult> SendWinningMessage(User user, Gift gift);
    }
}