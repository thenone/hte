﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Hte.Helpers.RandomHelper;
using Hte.Models;
using Hte.Models.Sms;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;

namespace Hte.Services.Messaging
{
    public class SmsSender : ISmsSender
    {
        private readonly HttpClient _httpClient;
        private readonly SmsOptions _smsOptions;
        private readonly IRandomGenerator _randomGenerator;
        private const string Signature = "\n" + "🌎 " + "کمک به زمین" + " 🌎";

        public SmsSender(IHttpClientFactory httpClientFactory, IOptions<Settings> options, IRandomGenerator randomGenerator)
        {
            _httpClient = httpClientFactory.CreateClient();
            _smsOptions = options.Value.SmsOptions;
            _randomGenerator = randomGenerator;
        }

        public async Task<SendSmsResult> SendLoginCode(string code, string to)
        {
            var body = new PtpSend(_smsOptions.UserName, _smsOptions.Password)
            {
                Message = "کد ورود شما:" + " " + $"{code}" + Signature,
                Numbers = $"{to}",
                SenderNumber = _smsOptions.SendNumber,
                SendType = SendType.Normal,
                YourMessageIds = $"{_randomGenerator.GetRandomNumericalString(10)}"
            };

            await SendPost(body);
            return new SendSmsResult();
        }

        public async Task<SendSmsResult> SendWinningMessage(User user, Gift gift)
        {
            var body = new PtpSend(_smsOptions.UserName, _smsOptions.Password)
            {
                Message = "🎉" + "تبریک" + "🎉" + "\n شما برنده‌ی جایزه " + $"{gift.Title}" + " شدید." + Signature,
                Numbers = $"{user.Mobile}",
                SenderNumber = _smsOptions.SendNumber,
                SendType = SendType.Normal,
                YourMessageIds = $"{_randomGenerator.GetRandomNumericalString(10)}"
            };

            await SendPost(body);
            return new SendSmsResult();
        }

        private async Task SendPost(PtpSend body)
        {
            var serialized = JsonConvert.SerializeObject(body);
            await _httpClient.PostAsync(
                _smsOptions.BaseSendUrl,
                new StringContent(
                    serialized,
                    Encoding.UTF8,
                    "application/json"
                )
            );
        }
    }
}