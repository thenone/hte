﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Hte.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Hte.Services
{
    public class TokenProvider
    {
        private readonly TokenOptions _tokenOptions;

        public TokenProvider(IOptions<Settings> options)
        {
            _tokenOptions = options.Value.TokenOptions;
        }

        public string GenerateToken(IEnumerable<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenOptions.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                _tokenOptions.Issuer,
                _tokenOptions.Audience,
                claims,
                expires: DateTime.Now.AddMinutes(double.Parse(_tokenOptions.ExpiresTimeInMinutes)),
                signingCredentials: creds
            );
            
            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }

        public string GetExpDate()
        {
            return DateTime.Now.AddMinutes(double.Parse(_tokenOptions.ExpiresTimeInMinutes)).ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
        }
    }
}