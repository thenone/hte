﻿using System.Linq;
using System.Security.Claims;
using Hte.Models;

namespace Hte.Services
{
    public class CurrentUserAuthorizer : ICurrentUserAuthorizer
    {
        public bool IsSelfUserAuthorized(ClaimsPrincipal user, string requestedUserId)
        {
            return GetUserId(user) == requestedUserId;
        }

        public string GetUserId(ClaimsPrincipal user)
        {
            return user.Claims.ToArray()[0].Value;
        }
    }
}