﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hte.Models;
using Hte.Models.Context;
using Microsoft.AspNetCore.DataProtection.XmlEncryption;
using Microsoft.AspNetCore.Hosting;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class GiftRepository : IGiftRepository
    {
        private readonly IMongoCollection<Gift> _collection;
        private readonly IHostingEnvironment _environment;
        
        public GiftRepository(IAppContext ctx, IHostingEnvironment hostingEnvironment, IUserRepository userRepository)
        {
            _collection = ctx.Gifts;
            _environment = hostingEnvironment;
        }
        
        public async Task<Gift> Insert(Gift gift)
        {
            await _collection.InsertOneAsync(gift);
            return gift;
        }

        public async Task<Gift> FindById(string id)
        {
            return await _collection.Find(Builders<Gift>.Filter.Eq(g => g.Id, id)).FirstOrDefaultAsync();
        }

        public async Task<Gift> DeletePhotoByIndex(Gift gift, int index)
        {
            var filters = Builders<Gift>.Filter.And(
                Builders<Gift>.Filter.Eq(g => g.Id, gift.Id),
                Builders<Gift>.Filter.Eq(g => g.Code, gift.Code)
            );

            var photo = gift.Photos.ToArray()[index];
            gift.Photos.RemoveAt(index);
            
            await _collection.UpdateOneAsync(filters, Builders<Gift>.Update.Set(g => g.Photos, gift.Photos));
            
            DeleteFileInGiftUploads(photo.Path);
            
            return gift;
        }

        public async Task<Gift> DeletePhotoByName(Gift gift, string name)
        {
            var filters = Builders<Gift>.Filter.And(
                Builders<Gift>.Filter.Eq(g => g.Id, gift.Id),
                Builders<Gift>.Filter.Eq(g => g.Code, gift.Code)
            );

            var photo = gift.Photos.First(ph => ph.Name == name);
            gift.Photos.Remove(photo);
            
            await _collection.ReplaceOneAsync(filters, gift);
            
            DeleteFileInGiftUploads(photo.Path);
            
            return gift;
        }

        public async Task<IEnumerable<Gift>> FindAsync(GiftDrawingStatus drawingStatus, int limit = 10, int offset = 0)
        {
            var filterDefinitions = new List<FilterDefinition<Gift>>();
            
            switch (drawingStatus)
            {
                case GiftDrawingStatus.Drawn:
                {
                    filterDefinitions.Add(Builders<Gift>.Filter.Eq(g => g.Drawed, true));
                    break;
                }
                case GiftDrawingStatus.Undrawn:
                {
                    filterDefinitions.Add(Builders<Gift>.Filter.Eq(g => g.Drawed, false));
                    break;
                }
                case GiftDrawingStatus.All:
                {
                    filterDefinitions.Add(Builders<Gift>.Filter.Exists(u => u.Id));
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(drawingStatus), drawingStatus, null);
            }

            return await _collection
                .Find(Builders<Gift>.Filter.And(filterDefinitions))
                .Limit(limit)
                .Skip(offset)
                .ToListAsync();
        }

        public async Task PushPhotos(Gift gift, IEnumerable<Photo> photos)
        {
            await _collection.UpdateOneAsync(Builders<Gift>.Filter.And(
                    Builders<Gift>.Filter.Eq(g => g.Id, gift.Id),
                    Builders<Gift>.Filter.Eq(g => g.Code, gift.Code)
                ),
                Builders<Gift>.Update.PushEach(g => g.Photos, photos)
            );
        }

        public async Task PushCandidate(Gift gift, DrawChance drawChance)
        {
            await _collection.UpdateOneAsync(
                Builders<Gift>.Filter.And(
                    Builders<Gift>.Filter.Eq(g => g.Id, gift.Id),
                    Builders<Gift>.Filter.Eq(g => g.Code, gift.Code)
                ),
                Builders<Gift>.Update.AddToSet(g => g.DrawChanceIds, drawChance.Id)
            );
        }

        public bool DoesGiftExists(string id, out Gift gift)
        {
            var result = _collection.Find(
                    Builders<Gift>.Filter.Eq(g => g.Id, id)
                )
                .Project(g => new Gift{Id = g.Id, DrawChanceIds = g.DrawChanceIds})
                .FirstOrDefaultAsync().Result;
            if (result == null)
            {
                gift = null;
                return false;
            }

            gift = result;
            return true;
        }

        public async Task<IEnumerable<Gift>> FindUserAvailableGifts(User user, int limit = 10, int offset = 10)
        {
            return await _collection
                .Find(Builders<Gift>.Filter.Lte(g => g.RequiredPoints, user.Points))
                .Limit(limit)
                .Skip(offset)
                .ToListAsync();
        }

        public async Task SetGiftDrawed(Gift gift, User winner)
        {
            await _collection.UpdateOneAsync(
                Builders<Gift>.Filter.Eq(g => gift.Id, gift.Id),
                Builders<Gift>.Update.Combine(
                    Builders<Gift>.Update.Set(g => g.Drawed, true),
                    Builders<Gift>.Update.Set(g => g.DateDrawed, DateTime.Now),
                    Builders<Gift>.Update.Set(g => g.WinnerId, winner.Id)
                )
            );
        }

        private void DeleteFileInGiftUploads(string filePath)
        {
            var pathToPhoto = Path.Combine(_environment.WebRootPath, filePath);
            if(File.Exists(pathToPhoto))
            {
                File.Delete(pathToPhoto);
            }
        }
    }
}