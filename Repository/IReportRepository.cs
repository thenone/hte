﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface IReportRepository
    {
        Task InsertAsync(UserReport report);
        Task<UserReport> FindByIdAsync(string id);
        Task<IEnumerable<UserReport>> FindAsync(int limit = 10, int offset = 0);
        Task<IEnumerable<UserReport>> FindBySeenAsync(bool seen, string userId, int limit = 10, int offset = 0);
        Task<bool> HasSeenReport(UserReport report, string userId);
        Task SeeReport(UserReport report, string userId);
    }
}