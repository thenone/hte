﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;
using Hte.Models.Context;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class DrawChanceRepository : IDrawChanceRepository
    {
        private readonly IAppContext _context;

        public DrawChanceRepository(IAppContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<DrawChance>> Find(int limit = 10, int skip = 0)
        {
            return await _context.DrawChances.Find(new BsonDocument()).Limit(limit).Skip(skip).ToListAsync();
        }

        public async Task<DrawChance> FindById(string id)
        {
            return await _context.DrawChances.Find(Builders<DrawChance>.Filter.Eq(dc => dc.Id, id)).FirstOrDefaultAsync();
        }

        public bool UserGiftDrawChanceExists(User user, Gift gift, out DrawChance drawChance)
        {
            var result = _context.DrawChances.Find(
                Builders<DrawChance>.Filter.And(
                    Builders<DrawChance>.Filter.Eq(dc => dc.Gift, gift.Id),
                    Builders<DrawChance>.Filter.Eq(dc => dc.User, user.Id)
                )
            ).FirstOrDefaultAsync().Result;

            if (result == null)
            {
                drawChance = null;
                return false;
            }

            drawChance = result;
            return true;
        }

        public async Task IncreaseDrawChancePointsByOne(DrawChance drawChance, int basePoint)
        {
            await _context.DrawChances.UpdateOneAsync(
                Builders<DrawChance>.Filter.Eq(dc => dc.Id, drawChance.Id),
                Builders<DrawChance>.Update.Combine(
                    Builders<DrawChance>.Update.Inc(dc => dc.Quantity, 1),
                    Builders<DrawChance>.Update.Inc(dc => dc.TotalPoints, basePoint)
                )
            );
        }

        public async Task<DrawChance> Insert(DrawChance drawChance)
        {
            await _context.DrawChances.InsertOneAsync(drawChance);
            return drawChance;
        }

        public async Task<IEnumerable<DrawChance>> FindGiftChances(Gift gift)
        {
            return await _context.DrawChances
                .Find(Builders<DrawChance>.Filter.Eq(dc => dc.Gift, gift.Id))
                .ToListAsync();
        }

        public async Task<IEnumerable<DrawChance>> FindUserChances(User user, int limit = 10, int offset = 0)
        {
            return await _context.DrawChances
                .Find(Builders<DrawChance>.Filter.Eq(dc => dc.User, user.Id))
                .Limit(limit)
                .Skip(offset)
                .ToListAsync();
        }
    }
}