﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hte.Models;
using Hte.Models.Context;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly IMongoCollection<Notification> _collection;

        public NotificationRepository(IAppContext context)
        {
            _collection = context.Notifications;
        }
        
        public async Task InsertAsync(Notification notification)
        {
            await _collection.InsertOneAsync(notification);
        }

        public async Task<Notification> FindByIdAsync(string id)
        {
            return await _collection.Find(Builders<Notification>.Filter.Eq(n => n.Id, id)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Notification>> FindUserNotificationsAsync(
            string userId,
            NotificationSeenStatus seenStatus = NotificationSeenStatus.All,
            int limit = 10,
            int offset = 0
        )
        {
            var filterDefinititions = new List<FilterDefinition<Notification>> {Builders<Notification>.Filter.AnyEq(n => n.UsersId, userId)};
            
            if (seenStatus == NotificationSeenStatus.Unseen)
            {
                filterDefinititions.Add(Builders<Notification>.Filter.AnyNe(n => n.SeenUserIds, userId));
            }
            else if (seenStatus == NotificationSeenStatus.Seen)
            {
                filterDefinititions.Add(Builders<Notification>.Filter.AnyEq(n => n.SeenUserIds, userId));
            }

            return await _collection
                .Find(Builders<Notification>.Filter.And(filterDefinititions))
                .Limit(limit)
                .Skip(offset)
                .ToListAsync();
        }

        public async Task<bool> HasUserSeenNotification(Notification notification, string userId)
        {
            return await _collection.Find(Builders<Notification>.Filter.And(
                           Builders<Notification>.Filter.Eq(n => n.Id, notification.Id),
                           Builders<Notification>.Filter.AnyEq(n => n.SeenUserIds, userId)
                       )
                   ).FirstOrDefaultAsync() != null;
        }

        public async Task SeeNotificationAsync(Notification notification, string userId)
        {
            await _collection.UpdateOneAsync(
                Builders<Notification>.Filter.Eq(n => n.Id, notification.Id),
                Builders<Notification>.Update.Combine(
                    Builders<Notification>.Update.Push(n => n.SeenUserIds, userId)
                )
            );
        }

        public async Task<bool> UserIsInNotificationAsync(Notification notification, string userId)
        {
            return await _collection.Find(
                       Builders<Notification>.Filter.And(
                           Builders<Notification>.Filter.Eq(n => n.Id, notification.Id),
                           Builders<Notification>.Filter.AnyEq(n => n.UsersId, userId)
                       )
                   ).FirstOrDefaultAsync() != null;
        }
    }
}