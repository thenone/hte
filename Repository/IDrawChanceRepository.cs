﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface IDrawChanceRepository
    {
        Task<IEnumerable<DrawChance>> Find(int limit = 10, int skip = 0);
        Task<DrawChance> FindById(string id);
        bool UserGiftDrawChanceExists(User user, Gift gift, out DrawChance drawChance);
        Task IncreaseDrawChancePointsByOne(DrawChance drawChance, int basePoint);
        Task<DrawChance> Insert(DrawChance drawChance);
        Task<IEnumerable<DrawChance>> FindGiftChances(Gift gift);
        Task<IEnumerable<DrawChance>> FindUserChances(User user, int limit = 10, int offset = 0);
    }
}