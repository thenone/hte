﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface IPostRepository
    {
        Task<Post> FindById(string id);
        Task<IEnumerable<Post>> Find(int limit = 10, int offset = 0);
        Task Insert(Post post);
        Task Update(string id, Post newPost);
        Task<IEnumerable<Post>> FindRecentAsync(int limit = 10, int offset = 0);
    }
}