﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;

namespace Hte.Repository
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> Find(int limit = 10, int skip = 0, bool useResposeProjection = false);
        Task<User> FindById(string id, bool useResposeProjection = false);
        Task<User> FindByMobile(string mobile, bool useResposeProjection = false);
        Task Insert(User user);
        Task<bool> HasUserDrawChance(User user, DrawChance drawChance);
        Task AddDrawChance(User user, DrawChance drawChance);
        Task DecreaseUserPoints(User user, int giftPoints);
        Task<IEnumerable<User>> FindGiftCandidates(Gift gift);
        Task AddUserCollecting(User user, TrashCollecting collecting);
        Task UpdateAsync(string id, User newUser);
        Task UpdateAvatarAsync(User user, Photo avatar);
    }
}