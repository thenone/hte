﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hte.Models;
using Hte.Models.Context;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Hte.Repository
{
    public class ReportRepository : IReportRepository
    {
        private readonly IMongoCollection<UserReport> _collection;

        public ReportRepository(IAppContext context)
        {
            _collection = context.UserReportComplaints;
        }
        
        public async Task InsertAsync(UserReport report)
        {
            await _collection.InsertOneAsync(report);
        }

        public async Task<UserReport> FindByIdAsync(string id)
        {
            return await _collection.Find(Builders<UserReport>.Filter.Eq(ur => ur.Id, id)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<UserReport>> FindAsync(int limit = 10, int offset = 0)
        {
            return await _collection.Find(new BsonDocument()).Limit(limit).Skip(offset).ToListAsync();
        }

        public async Task<IEnumerable<UserReport>> FindBySeenAsync(bool seen, string userId, int limit = 10, int offset = 0)
        {
            var filterDefinition = Builders<UserReport>.Filter.AnyEq(ur => ur.SeenUserIds, userId);
            if (!seen)
            {
                filterDefinition = Builders<UserReport>.Filter.AnyNe(ur => ur.SeenUserIds, userId);
            }
            
            return await _collection.Find(filterDefinition).Limit(limit).Skip(offset).ToListAsync();
        }

        public async Task<bool> HasSeenReport(UserReport report, string userId)
        {
            return await _collection
                       .Find(Builders<UserReport>.Filter.Eq(ur => ur.Id, report.Id))
                       .Project(Builders<UserReport>.Projection.Include(ur => ur.Id))
                       .FirstOrDefaultAsync() != null;
        }

        public async Task SeeReport(UserReport report, string userId)
        {
            await _collection.UpdateOneAsync(
                Builders<UserReport>.Filter.Eq(ur => ur.Id, report.Id),
                Builders<UserReport>.Update.Push(ur => ur.SeenUserIds, userId)
            );
        }
    }
}