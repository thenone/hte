﻿using Hte.Models;

namespace Hte
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public TokenOptions TokenOptions { get; set;  }
        public AuthOptions AuthOptions { get; set; }
        public SmsOptions SmsOptions { get; set; }
    }
}