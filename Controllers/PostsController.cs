﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Models;
using Hte.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Hte.Controllers
{
    [Route("api/[controller]")]
    public class PostsController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly string _wwwroot;

        public PostsController(IHostingEnvironment hostingEnvironment, IPostRepository postRepository)
        {
            _postRepository = postRepository;
            _wwwroot = hostingEnvironment.WebRootPath;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([FromForm] Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            if (post.Files != null)
            {
                var media = new List<Photo>();
                foreach (var file in post.Files)
                {
                    if (file.Length <= 0) continue;
                    if (
                        file.ContentType != "image/png" &&
                        file.ContentType != "image/jpeg" &&
                        file.ContentType != "video/mp4" &&
                        file.ContentType != "video/3gpp" &&
                        file.ContentType != "video/x-msvideo" &&
                        file.ContentType != "video/x-ms-wmv"
                    )
                    {
                        return BadRequest(new {message = "file type not supported"});
                    }

                    var uploadPath = Path.Combine(_wwwroot, "uploads", "posts", post.Code, file.FileName);
                
                    Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(uploadPath)));
                
                    Path.GetRelativePath(_wwwroot, uploadPath);
                
                    using (var fileStream = new FileStream(uploadPath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                
                    media.Add(new Photo{Ext = file.ContentType, Name = file.FileName, Path = Path.GetRelativePath(_wwwroot, uploadPath)});
                }

                post.Media = media;
            }

            await _postRepository.Insert(post);

            return CreatedAtRoute("GetPost", new {id = post.Id}, post);
        }
        
        [HttpPatch("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit([FromRoute] [Required] [MongoObjectId] string id, [FromBody] JsonPatchDocument<Post> post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var oldPost = await _postRepository.FindById(id);
            if (oldPost == null)
            {
                return NotFound(new {message = "post not found"});
            }

            try
            {
                post.ApplyTo(oldPost);
            }
            catch (JsonPatchException)
            {
                return BadRequest(new {message = "invalid requested path"});
            }

            await _postRepository.Update(oldPost.Id, oldPost);

            return NoContent();
        }
        
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit([FromRoute] [Required] [MongoObjectId] string id, [FromForm] Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var oldPost = await _postRepository.FindById(id);
            if (oldPost == null)
            {
                return NotFound(new {message = "post not found"});
            }

            await _postRepository.Update(oldPost.Id, post);

            return NoContent();
        }

        [HttpGet("{id}", Name = "GetPost")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> GetPost([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var post = await _postRepository.FindById(id);
            if (post == null)
            {
                return NotFound(new {message = "post not found"});
            }

            return Ok(post);
        }

        [HttpGet("", Name = "GetRecentPosts")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> GetRecents([FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid limit/offset provided"});
            }

            return Ok(await _postRepository.FindRecentAsync(limit, offset));
        }
    }
}