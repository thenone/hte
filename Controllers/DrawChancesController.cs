﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Controllers
{
    [Route("api/draw_chances")]
    public class DrawChancesController : Controller
    {
        private readonly IDrawChanceRepository _drawChanceRepository;
        
        public DrawChancesController(IDrawChanceRepository drawChanceRepository)
        {
            _drawChanceRepository = drawChanceRepository;
        }

        [HttpGet("", Name = "GetDrawChances")]
        public async Task<IActionResult> Get([FromQuery] int limit = 10, [FromQuery] int offset = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var drawChance = await _drawChanceRepository.Find(limit, offset);

            return Ok(drawChance);
        }
        
        [HttpGet("{id}", Name = "GetDrawChanceItem")]
        public async Task<IActionResult> GetItem([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var drawChance = await _drawChanceRepository.FindById(id);
            if (drawChance == null)
            {
                return NotFound(new {message = "draw chance not found"});
            }

            return Ok(drawChance);
        }
    }
}