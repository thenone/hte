﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Models;
using Hte.Models.Form;
using Hte.Repository;
using Hte.Services;
using Hte.Services.Mapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Hte.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly IGiftRepository _giftRepository;
        private readonly IDrawChanceRepository _drawChanceRepository;
        private readonly ICollectingRepository _collectingRepository;
        private readonly IRepositoryResponseMapper _mapper;
        private readonly ICurrentUserAuthorizer _currentUserAuthorizer;
        private readonly UserManager<User> _userManager;
        private readonly string _wwwroot;

        public UsersController(
            IHostingEnvironment hostingEnvironment,
            IUserRepository userRepository,
            IUserGroupRepository userGroupRepository,
            IGiftRepository giftRepository,
            IDrawChanceRepository drawChanceRepository,
            ICollectingRepository collectingRepository,
            IRepositoryResponseMapper mapper,
            ICurrentUserAuthorizer currentUserAuthorizer,
            UserManager<User> userManager
        )
        {
            _userRepository = userRepository;
            _userGroupRepository = userGroupRepository;
            _giftRepository = giftRepository;
            _drawChanceRepository = drawChanceRepository;
            _collectingRepository = collectingRepository;
            _mapper = mapper;
            _currentUserAuthorizer = currentUserAuthorizer;
            _userManager = userManager;
            _wwwroot = hostingEnvironment.WebRootPath;
        }

        [HttpGet("")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetList([FromQuery]int limit = 10, [FromQuery]int offset = 0)
        {
            var users = _mapper.MapUserResponses(await _userRepository.Find(limit, offset));
            return Ok(users);
        }

        [HttpGet("{id}", Name = "GetUser")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> GetUser([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (User.IsInRole("User") && !_currentUserAuthorizer.IsSelfUserAuthorized(User, id))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var user = await _userRepository.FindById(id);
            return Ok(_mapper.MapUserResponse(user));
        }
        
        [HttpPatch("{id}")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> EditUser(
            [FromForm] [Required] UserEditForm form,
            [FromRoute] [Required] [MongoObjectId] string id
        )
        {
            if (User.IsInRole("User") && !_currentUserAuthorizer.IsSelfUserAuthorized(User, id))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var oldUser = await _userRepository.FindById(id);
            if (oldUser == null)
            {
                return NotFound(new {message = "user not found"});
            }

            if (!string.IsNullOrEmpty(form.NewPassword) && form.NewPassword != form.ConfirmPassword)
            {
                return BadRequest(new {message = "not match password confirmation"});
            }

            if (
                !string.IsNullOrEmpty(form.OldPassword) &&
                !string.IsNullOrEmpty(form.NewPassword) &&
                !string.IsNullOrEmpty(form.ConfirmPassword) &&
                form.NewPassword == form.ConfirmPassword
            )
            {
                var identityResult = await _userManager.ChangePasswordAsync(oldUser, form.OldPassword, form.NewPassword);
                if (!identityResult.Succeeded)
                {
                    return BadRequest(new {message = "invalid new/old password provided"});
                }
            }

            await _userRepository.UpdateAsync(oldUser.Id, _mapper.MapEditForm(form));
            return NoContent();
        }

        [HttpGet("{id}/groups")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetUserGroups([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {mesage = "invalid id"});
            }

            var user = await _userRepository.FindById(id, true);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            return Ok(await _userGroupRepository.FindUserGroups(user));
        }

        [HttpPost("{user_id}/draws")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> CreateUserDrawChance(
            [FromRoute(Name = "user_id")] [Required] [MongoObjectId]
            string userId,
            [FromForm(Name = "gift_id")] [Required] [MongoObjectId]
            string giftId
        )
        {
            if (User.IsInRole("User") && !_currentUserAuthorizer.IsSelfUserAuthorized(User, userId))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var user = await _userRepository.FindById(userId);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            var gift = await _giftRepository.FindById(giftId);
            if (gift == null)
            {
                return NotFound(new {message = "gift not found"});
            }

            if (user.Points < gift.RequiredPoints)
            {
                return BadRequest(new {message = "user does not have required points"});
            }

            var drawChanceExistsBefore = _drawChanceRepository.UserGiftDrawChanceExists(user, gift, out var drawChance);
            if (drawChanceExistsBefore)
            {
                Task.WaitAll(
                    _drawChanceRepository.IncreaseDrawChancePointsByOne(drawChance, gift.RequiredPoints),
                    _userRepository.DecreaseUserPoints(user, gift.RequiredPoints)
                );
            }
            else
            {
                drawChance = new DrawChance
                {
                    DateBought = DateTime.Now,
                    Gift = gift.Id,
                    GiftUnitPoint = gift.RequiredPoints,
                    Quantity = 1,
                    TotalPoints = gift.RequiredPoints,
                    User = user.Id
                };
                drawChance = await _drawChanceRepository.Insert(drawChance);
                Task.WaitAll(
                    _giftRepository.PushCandidate(gift, drawChance),
                    _userRepository.AddDrawChance(user, drawChance),
                    _userRepository.DecreaseUserPoints(user, gift.RequiredPoints)
                );
            }

            return CreatedAtRoute("GetDrawChanceItem", new {id = drawChance.Id}, drawChance);
        }

        [HttpGet("{id}/available_gifts")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> GetAvailableGifts(
            [FromRoute(Name = "id")] [Required] [MongoObjectId] string userId,
            [FromQuery] int limit = 10,
            [FromQuery] int offset = 0
        )
        {
            if (User.IsInRole("User") && !_currentUserAuthorizer.IsSelfUserAuthorized(User, userId))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var user = await _userRepository.FindById(userId);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            return Ok(await _giftRepository.FindUserAvailableGifts(user, limit, offset));
        }

        [HttpPost("{id}/collectings")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> NewUserCollecting(
            [FromRoute(Name = "id")] [Required] [MongoObjectId] string userId,
            TrashCollecting trashCollecting
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var user = await _userRepository.FindById(userId);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            trashCollecting.Id = ObjectId.GenerateNewId(DateTime.Now).ToString();
            trashCollecting.UserId = user.Id;
            Task.WaitAll(
                _userRepository.AddUserCollecting(user, trashCollecting),
                _collectingRepository.Insert(trashCollecting)
            );

            return CreatedAtRoute("GetCollecting", new {id = trashCollecting.Id}, trashCollecting);
        }

        [HttpGet("{id}/chances", Name = "GetUserChances")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetUserChances(
            [FromRoute] [Required] [MongoObjectId] string id,
            [FromQuery] int limit = 10,
            [FromQuery] int offset = 0
        )
        {
            if (!_currentUserAuthorizer.IsSelfUserAuthorized(User, id))
            {
                return Unauthorized();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            var user = await _userRepository.FindById(id);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }
            
            return Ok(await _drawChanceRepository.FindUserChances(user, limit, offset));
        }

        [HttpPut("{id}/avatar")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> SetAvatar(
            [FromRoute] [Required] [MongoObjectId] string id,
            [FromForm] FileUpload fileUpload
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }

            if (!_currentUserAuthorizer.IsSelfUserAuthorized(User, id))
            {
                return Forbid();
            }

            var user = await _userRepository.FindById(id);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            var file = fileUpload.File;
            if (file.Length <= 0) return BadRequest(new {message = "invalid uploaded file"});
            if (file.ContentType != "image/png" && file.ContentType != "image/jpg")
            {
                return BadRequest(new {message = "uploaded file type not supported"});
            }

            var uploadPath = Path.Combine(_wwwroot, "uploads", "users", user.Id, file.FileName);

            Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(uploadPath)));

            Path.GetRelativePath(_wwwroot, uploadPath);

            using (var fileStream = new FileStream(uploadPath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            var avatar = new Photo
            {
                Ext = file.ContentType,
                Name = file.FileName,
                Path = Path.GetRelativePath(_wwwroot, uploadPath)
            };

            await _userRepository.UpdateAvatarAsync(user, avatar);
            return NoContent();
        }
    }
}