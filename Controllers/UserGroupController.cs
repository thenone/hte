﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Repository;
using Hte.Services.Mapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Controllers
{
    [Route("api/user_groups")]
    public class UserGroupController : Controller
    {
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly IUserRepository _userRepository;
        private readonly IRepositoryResponseMapper _repositoryResponseMapper;
        
        public UserGroupController(
            IUserGroupRepository userGroupRepository,
            IUserRepository userRepository,
            IRepositoryResponseMapper repositoryResponseMapper
        )
        {
            _userGroupRepository = userGroupRepository;
            _userRepository = userRepository;
            _repositoryResponseMapper = repositoryResponseMapper;
        }

        [HttpPost("")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([FromForm] [Required] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            var ug = await _userGroupRepository.Create(name);
            return CreatedAtRoute("GetUserGroup", new {id = ug.Id}, ug);
        }

        [HttpGet("{id}", Name = "GetUserGroup")]
        public async Task<IActionResult> GetById([Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }
            
            var ug = await _userGroupRepository.FindById(id);
            if (ug == null)
            {
                return NotFound();
            }

            return Ok(ug);
        }

        [HttpPut("{group_id}/users/{user_id}/assign")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AssignUser(
            [FromRoute(Name = "group_id")] [Required] [MongoObjectId] string groupId,
            [FromRoute(Name = "user_id")] [Required] [MongoObjectId] string userId
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            
            var group = await _userGroupRepository.FindById(groupId);
            if (group == null)
            {
                return NotFound(new {message = "group not found"});
            }

            var user = await _userRepository.FindById(userId, true);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            if (!await _userGroupRepository.UserExistsInGroup(group, user))
            {
                _userGroupRepository.AssignUser(group, user);
            }
            
            return NoContent();
        }

        [HttpPut("{group_id}/users/{user_id}/unassign")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UnAssignUser(
            [FromRoute(Name = "group_id")] [Required] [MongoObjectId] string groupId,
            [FromRoute(Name = "user_id")] [Required] [MongoObjectId] string userId
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            
            var group = await _userGroupRepository.FindById(groupId);
            if (group == null)
            {
                return NotFound(new {message = "group not found"});
            }

            var user = await _userRepository.FindById(userId, true);
            if (user == null)
            {
                return NotFound(new {message = "user not found"});
            }

            if (!await _userGroupRepository.UserExistsInGroup(group, user))
            {
                return BadRequest(new {message = "user does not exist in the group"});
            }
            
            await _userGroupRepository.UnAssignUserAsync(group, user);
            return NoContent();
        }

        [HttpGet("{id}/users")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetGroupUsers([Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {mesage = "invalid id"});
            }
            
            var group = await _userGroupRepository.FindById(id);
            if (group == null)
            {
                return NotFound(new {message = "group not found"});
            }

            return Ok(_repositoryResponseMapper.MapUserResponses(await _userGroupRepository.FindGroupUsers(group))); 
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditGroupName(
            [FromRoute] [Required] [MongoObjectId] string id,
            [FromForm] [Required] string name
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            
            var group = await _userGroupRepository.FindById(id);
            if (group == null)
            {
                return NotFound(new {message = "user group not found"});
            }

            await _userGroupRepository.UpdateGroupName(group, name);
            return NoContent();
        }
    }
}