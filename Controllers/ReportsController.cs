﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Hte.Helpers.Validators;
using Hte.Models;
using Hte.Repository;
using Hte.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hte.Controllers
{
    [Route("api/[controller]")]
    public class ReportsController : Controller
    {
        private readonly IReportRepository _reportRepository;
        private readonly ICurrentUserAuthorizer _currentUserAuthorizer;

        public ReportsController(IReportRepository reportRepository, ICurrentUserAuthorizer currentUserAuthorizer)
        {
            _reportRepository = reportRepository;
            _currentUserAuthorizer = currentUserAuthorizer;
        }

        [HttpPost("")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Create([FromForm] UserReport form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid request"});
            }
            
            form.DateCreted = DateTime.Now;
            form.User = _currentUserAuthorizer.GetUserId(User);
            await _reportRepository.InsertAsync(form);
            return CreatedAtRoute("GetReport", new {id = form.Id}, form);
        }

        [HttpGet("{id}", Name = "GetReport")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetReport([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid id"});
            }

            var report = await _reportRepository.FindByIdAsync(id);
            if (report == null)
            {
                return NotFound(new {message = "user report bot found"});
            }
            
            return Ok(report);
        }

        [HttpGet(Name = "GetReports")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetList(
            [FromQuery(Name = "seen_status")] UserReportSeenStatus seenStatus = UserReportSeenStatus.All,
            [FromQuery] int limit = 10,
            [FromQuery] int offset = 0
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid offset/limit"});
            }

            if (seenStatus != UserReportSeenStatus.All)
            {
                return Ok(
                    await _reportRepository.FindBySeenAsync(
                        seenStatus == UserReportSeenStatus.Seen,
                        _currentUserAuthorizer.GetUserId(User),
                        limit,
                        offset
                    )
                );
            }

            return Ok(await _reportRepository.FindAsync(limit, offset));
        }

        [HttpPut("{id}/see")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> SeeReport([FromRoute] [Required] [MongoObjectId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new {message = "invalid report id"});
            }

            var report = await _reportRepository.FindByIdAsync(id);
            if (report == null)
            {
                return NotFound(new {message = "user report not found"});
            }

            var currentUserId = _currentUserAuthorizer.GetUserId(User);

            if (await _reportRepository.HasSeenReport(report, currentUserId))
            {
                return BadRequest(new {message = "user has already seen this report item"});
            }

            await _reportRepository.SeeReport(report, currentUserId);
            return NoContent();
        }
    }
}